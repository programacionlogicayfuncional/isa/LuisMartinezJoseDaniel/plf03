(ns plf03.core)
(defn función-comp-1
  []
  (let [f (fn [x] (dec x))
        g (fn [x y] (- x y))
        z (comp f g)]
    (z 30 2)))
(defn función-comp-2
  []
  (let [f (fn [x] (dec x))
        g (fn [x] (inc x))
        z (comp f g)]
    (z 100)))
(defn función-comp-3
  []
  (let [f (fn [x] (* x x))
        g (fn [x y] (+ x y))
        z (comp f g)]
    (z 15 3)))
(defn función-comp-4
  []
  (let [f (fn [x] (boolean? x))
        g (fn [x] (pos? x))
        z (comp f g)]
    (z -3)))
(defn función-comp-5
  []
  (let [f (fn [xs] (map dec xs))
        g (fn [xs] (filter odd? xs))
        z (comp f g)]
    (z [1 2 3 4 5 6 7 8 9])))
(defn función-comp-6
  []
  (let [f (fn [xs] (filter pos? xs))
        g (fn [xs] (sort xs))
        z (comp f g)]
    (z [-5 -4 -3 3 5 6])))
(defn función-comp-7
  []
  (let [f (fn [xs] (filter neg? xs))
        g (fn [x y] (range x y))
        z (comp f g)]
    (z -10 10)))
(defn función-comp-8
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (map inc xs))
        h (fn [xs] (filter pos? xs))
        z (comp f g h)]
    (z [1 -2 -3 4 10 34 102 -1 501 0])))
(defn función-comp-9
  []
  (let [f (fn [xs] (reverse xs))
        g (fn [xs] (filter string? xs))
        z (comp f g)]
    (z [10 20 "HOLA" "MUNDO" 30 40 "CLOJURE"])))
(defn función-comp-10
  []
  (let [f (fn [xs] (distinct  xs))
        g (fn [xs] (filter neg? xs))
        z (comp f g)]
    (z [-1 -1 -2 -4 -5 10 30 -2])))
(defn función-comp-11
  []
  (let [f (fn [x] (double? x))
        g (fn [x y] (quot x y))
        z (comp f g)]
    (z 10.5 3)))
(defn función-comp-12
  []
  (let [f (fn [x] (true? x))
        g (fn [x y] (= x y))
        z (comp f g)]
    (z "a" \a )))
(defn función-comp-13
  []
  (let [f (fn [xs] (count xs))
        g (fn [x y] (cons x y))
        z (comp f g)]
    (z {:k1 20 :k2 30 :k3 -4} [1 2])))
(defn función-comp-14
  []
  (let [f (fn [xs] (filter string? xs))
        g (fn [xs y] (disj xs y))
        z (comp f g)]
    (z #{1 2 3 4 "one" "two" "three"} 1)))
(defn función-comp-15
  []
  (let [f (fn [xs] (sort xs))
        g (fn [xs] (filter double? xs))
        h (fn [xs] (filter number? xs))
        z (comp f g h)]
    (z [1 -2.0 -3.0 4 10 34 1.02 -1 501 0.4 "A" "B" 0.23 -12])))
(defn función-comp-16
  []
  (let [f (fn [xs] (apply str xs))
        g (fn [xs] (reverse xs))
        z (comp f g)]
    (z "odnuM aloH ")))
(defn función-comp-17
  []
  (let [f (fn [xs] (sort > xs))
        g (fn [xs ys] (into xs ys))
        z (comp f g)]
    (z #{} [1 2 3 4 5 10 -23 -1 -2 3])))
(defn función-comp-18
  []
  (let [f (fn [xs] (apply str xs))
        g (fn [xs ys] (concat xs ys))
        z (comp f g)]
    (z #{"Number " "New " } '(1 2 3 4))))
(defn función-comp-19
  []
  (let [f (fn [x] (str x))
        g (fn [xs] (count xs))
        z (comp f g)]
    (z "Hola Danny")))
(defn función-comp-20
  []
  (let [f (fn [xs] (into [] xs))
        g (fn [xs] (filter number? xs))
        h (fn [xs] (vals xs))
        z (comp f g h)]
    (z {:k1 23 :k2 34 :k3 "oso" :k4 true :k5 -3/2})))
(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-7)
(función-comp-6)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

;;complement
(defn función-complement-1
  []
  (let [f (fn [x] (pos? x))
        z (complement f)]
    (z 90)))
(defn función-complement-2
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z \a)))
(defn función-complement-3
  []
  (let [f (fn [x] (odd? x))
        z (complement f )]
    (z 240193)))
(defn función-complement-4
  []
  (let [f (fn [x] (even? x))
        z (complement f)]
    (z 240193)))
(defn función-complement-5
  []
  (let [f (fn [x y] (= x y))
        z (complement f)]
    (z 12 12)))
(defn función-complement-6
  []
  (let [f (fn [xs] (coll? xs))
        z (complement f)]
    (z "Coleccion") ))
(defn función-complement-7
  []
  (let [f (fn [x] ((complement even?) x))
        z (complement f)]
    (z 14)))
(defn función-complement-8
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z true)))
(defn función-complement-9
  []
  (let [f (fn [xs] (associative? xs))
        z (complement f)]
    (z ["A" "B" 1 2])))
(defn función-complement-10
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z "a")))
(defn función-complement-11
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z "1.23")))
(defn función-complement-12
  []
  (let [f (fn [x] (decimal? x))
        z (complement f)]
    (z 1020)))
(defn función-complement-13
  []
  (let [f (fn [x] (double? x))
        z (complement f)]
    (z -12)))
(defn función-complement-14
  []
  (let [f (fn [x] (ident? x))
        z (complement f)]
    (z :k1)))
(defn función-complement-15
  []
  (let [f (fn [x] (keyword? x))
        z (complement f)]
    (z ":k1")))
(defn función-complement-16
  []
  (let [f (fn [xs] (list? xs))
        z (complement f)]
    (z [])))
(defn función-complement-17
  []
  (let [f (fn [x y] (map-entry? (x y)))
        z (complement f)]
    (z :k1 "A")))
(defn función-complement-18
  []
  (let [f (fn [xs] (map? xs))
        z (complement f)]
    (z {:k1 "B" :k2 "C"})))
(defn función-complement-19
  []
  (let [f (fn [x y z] (> x y z))
        z (complement f)]
    (z 10 20 30)))
(defn función-complement-20
  []
  (let [f (fn [w x y z] (= (- w x) (+ y z)))
        z (complement f)]
    (z 60 10 50 10)))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)
;;constantly
(defn función-constantly-1
  []
  (let [f true
        z (constantly f)]
    (z 60)))
(defn función-constantly-2
  []
  (let [f nil
        z (constantly f)]
    (z 60 70)))
(defn función-constantly-3
  []
  (let [f "constantly"
        z (constantly f)]
    (z [ 1 2 3 4] )))
(defn función-constantly-4
  []
  (let [pi 3.1416
        z (constantly pi)]
    (z '())))
(defn función-constantly-5
  []
  (let [f [false false true false]
        z (constantly f)]
    (z [true true])))
(defn función-constantly-6
  []
  (let [f (hash-set "A" "B" "C")
        z (constantly f)]
    (z nil)))
(defn función-constantly-7
  []
  (let [f (hash-set "A" "B" "C")
        g (vector "A" "B" "C" "D")
        h (concat f g)
        z (constantly h)]
    (z { :k1 1 :k2 2})))
(defn función-constantly-8
  []
  (let [f {:k1 "Say so" :k2 " Again"}
        z (constantly f)]
    (z true)))
(defn función-constantly-9
  []
  (let [f '([] [])
        z (constantly f)]
    (z false)))
(defn función-constantly-10
  []
  (let [f #{nil}
        z (constantly f)]
    (z #{:k1 1 :k2 2})))
(defn función-constantly-11
  []
  (let [f ["constantly 2"]
           z (constantly f)]
    (z 1/4)))
(defn función-constantly-12
  []
  (let [f (range 10 100 5)
        z (constantly f)]
    (z [1 2 3 4 5])))
(defn función-constantly-13
  []
  (let [f (str true)
        z (constantly f)]
    (z false "false")))
(defn función-constantly-14
  []
  (let [f '("The same")
        z (constantly f)]
    (z 1 2 3 4)))
(defn función-constantly-15
  []
  (let [f (first #{1 2 3 4})
        z (constantly f)]
    (z nil false true)))
(defn función-constantly-16
  []
  (let [f \a
        z (constantly f)]
    (z false true)))
(defn función-constantly-17
  []
  (let [f :k1
        z (constantly f)]
    (z {:k2 2})))
(defn función-constantly-18
  []
  (let [f '(:k2 true)
        z (constantly f)]
    (z 90 89 70)))
(defn función-constantly-19
  []
  (let [f (vector false true :k10 :k11)
        z (constantly f)]
    (z '([] []))))
(defn función-constantly-20
  []
  (let [f #{[1] '(2) {:k3 3} #{4}}
        z (constantly f)]
    (z 1 2 3 90 100 1000)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)
;;every-pred
(defn función-every-pred-1
  []
  (let [f (fn [x] (float? x))
        z (every-pred f)]
    (z 3)))
(defn función-every-pred-2
  []
  (let [f (fn [x] (int? x))
        z (every-pred f)]
    (z 90)))
(defn función-every-pred-3
  []
  (let [f (fn [x] (neg? x))
        z (every-pred f)]
    (z -0)))
(defn función-every-pred-4
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (neg? x))
        h (fn [x] (ratio? x))
        z (every-pred f g h)]
    (z -12/3)))
(defn función-every-pred-5
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (odd? x))
        z (every-pred f g)]
    (z 20)))
(defn función-every-pred-6
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (string? x))
        z (every-pred f g)]
    (z "12")))
(defn función-every-pred-7
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (double? x))
        h (fn [x] (float? x))
        z (every-pred f g h)]
    (z 12.98)))
(defn función-every-pred-8
  []
  (let [f (fn [xs] (coll? xs))
        g (fn [xs] (string? xs))
        z (every-pred f g)]
    (z [1 2 2 5 6])))
(defn función-every-pred-9
  []
  (let [f (fn [x] (char? x))
        g (fn [x] (number? x))
        h (fn [x] (vector? x))
        z (every-pred h f g)]
    (z "HI DANNY")))
(defn función-every-pred-10
  []
  (let [f (fn [xs] (coll? xs))
        g (fn [xs] (list? xs))
        h (fn [xs] (seq? xs))
        z (every-pred f g h)]
    (z '(10 20 30))))
(defn función-every-pred-11
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (keyword? x))
        z (every-pred f g)]
    (z {:k1 10 })))
(defn función-every-pred-12
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (nat-int? x))
        z (every-pred f g)]
    (z 1023)))
(defn función-every-pred-13
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (ratio? x))
        z (every-pred f g)]
    (z 2/3)))
(defn función-every-pred-14
  []
  (let [f (fn [x] (rational? x))
        g (fn [x] (ratio? x))
        z (every-pred f g)]
    (z 0.23)))
(defn función-every-pred-15
  []
  (let [f (fn [xs] (seq? xs))
        g (fn [xs] (vector? xs))
        z (every-pred f g)]
    (z ["A" "B" "C"])))
(defn función-every-pred-16
  []
  (let [f (fn [xs] (seqable? xs))
        g (fn [xs] (map? xs))
        z (every-pred f g)]
    (z {:k1 10 :k2 20})))
(defn función-every-pred-17
  []
  (let [f (fn [xs] (seqable? xs))
        g (fn [xs] (seq? xs))
        h (fn [xs] (list? xs))
        z (every-pred f g h)]
    (z '(:k1 true false 100))))
(defn función-every-pred-18
  []
  (let [f (fn [xs] (coll? xs))
        g (fn [xs] (set? xs))        
        z (every-pred f g)]
    (z (hash-set 10 30 40 "DO I WANNA KNOW?"))))
(defn función-every-pred-19
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (some? x))
        z (every-pred f g)]
    (z "")))
(defn función-every-pred-20
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (pos-int? x))
        z (every-pred f g)]
    (z 1000)))
(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)
;;fnil
(defn función-fnil-1
  []
  (let [f (fn [x] (inc x))        
        z (fnil f 0)]
    (z nil)))
(defn función-fnil-2
  []
  (let [f (fn [xs] (vector? xs))
        z (fnil f nil)]
    (z ["A" "B"])))
(defn función-fnil-3
  []
  (let [f (fn [x] (+ x x))
        z (fnil f 10)]
    (z nil)))
(defn función-fnil-4
  []
  (let [f (fn [x y] (str x y))
        z (fnil f "Hello")]
    (z nil " Don")))
(defn función-fnil-5
  []
  (let [f (fn [xs ys] (concat xs ys))
        z (fnil f [])]
    (z nil {:k1 "1" :k2 "2"} )))
(defn función-fnil-6
  []
  (let [f (fn [xs ys] (into xs ys))
        z (fnil f [])]
    (z nil '(10 20))))
(defn función-fnil-7
  []
  (let [f (fn [x y] (str x y))
        z (fnil f "your number is ")]
    (z nil 777)))
(defn función-fnil-8
  []
  (let [f (fn [w x] (- w x))
        z (fnil f 0)]
    (z nil 100)))
(defn función-fnil-9
  []
  (let [f (fn [w x y] (- w x y))
        z (fnil f 0 1 2)]
    (z  100 nil nil)))
(defn función-fnil-10
  []
  (let [f (fn [x] (reverse x))
        z (fnil f "reverse")]
    (z nil)))
(defn función-fnil-11
  []
  (let [f (fn [x y z] (str x y z))
        z (fnil f "WELCOME ")]
    (z nil "DR. " "STRANGE")))
(defn función-fnil-12
  []
  (let [f (fn [x y] (/ x y))
        z (fnil f 3)]
    (z nil 98)))
(defn función-fnil-13
  []
  (let [f (fn [w x y] (= w x y))
        z (fnil f 0 0 0)]
    (z nil 0 nil)))
(defn función-fnil-14
  []
  (let [f (fn [x] (nat-int? x))
        z (fnil f 10)]
    (z nil)))
(defn función-fnil-15
  []
  (let [f (fn [xs] (filter string? xs))
        z (fnil f '("Nothing" "here" 1 2 3))]
    (z nil)))
(defn función-fnil-16
  []
  (let [f (fn [xs] (filter even? xs))
        z (fnil f '())]
    (z [-2 -3 -4 -6])))
(defn función-fnil-17
  []
  (let [f (fn [x] (number? x))
        z (fnil f 0)]
    (z nil)))
(defn función-fnil-18
  []
  (let [f (fn [w x y] (* x w y))
        z (fnil f 10 20 30)]
    (z nil 1 2)))
(defn función-fnil-19
  []
  (let [f (fn [w x y] (max x w y))
        z (fnil f 100 120 10)]
    (z nil nil 20)))
(defn función-fnil-20
  []
  (let [f (fn [xs] (drop-last xs))
        z (fnil f [0 1 2])]
    (z nil)))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)
;;juxt
(defn función-juxt-1
  []
  (let [f (fn [x] (identity x))
        z (juxt f)]
    (z :llave1)))
(función-juxt-1)
(defn función-juxt-2
  []
  (let [f (fn [x] (first x))
        z (juxt f)]
    (z "HOLA")))
(defn función-juxt-3
  []
  (let [f (fn [x] (first x))
        g (fn [x] (last x))
        z (juxt f g)]
    (z "DANNY")))
(defn función-juxt-4
  []
  (let [f (fn [w x y] (max w x y))
        g (fn [w x y] (min w x y))
        z (juxt f g)]
    (z 30 20 -10)))
(defn función-juxt-5
  []
  (let [f (fn [xs] (filter keyword? xs))
        g (fn [xs] (filter number? xs))
        z (juxt f g)]
    (z #{:k1 2 :k3 10 :k2 90 10.01 :k4 :k6 1.2})))
(defn función-juxt-6
  []
  (let [f (fn [xs] (filter string? xs))
        g (fn [xs] (filter char? xs))
        z (juxt f g)]
    (z '(1 2 \a \c "A" "B" true))))
(defn función-juxt-7
  []
  (let [f (fn [xs] (shuffle xs))
        g (fn [xs] (first xs))
        z (juxt g f)]
    (z '(true false true "true" "false" 0 1))))
(defn función-juxt-8
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (* x x))
        h (fn [x] (str x))
        z (juxt h g f)]
    (z -10)))
(defn función-juxt-9
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [xs] (first xs))
        h (fn [xs] (last xs))
        z (juxt f g h)]
    (z [-3 -1 -10 9 11 32 34 36 7])))
(defn función-juxt-10
  []
  (let [f (fn [xs] (filter string? xs))
        g (fn [xs] (filter int? xs))
        z (juxt f g)]
    (z '("AGENT"  true false 0 0 1.76 10.3 7))))
(defn función-juxt-11
  []
  (let [f (fn [xs] (sort xs))
        g (fn [xs] (filter even? xs))
        z (juxt f g)]
    (z [ 10 2 10 11 -3 2 9 1 18 -13 12])))
(defn función-juxt-12
  []
  (let [f (fn [x y] (str x y))
        g (fn [x y] (+ x y))
        z (juxt f g)]
    (z 20 20)))
(defn función-juxt-13
  []
  (let [f (fn [x y] (vector x y))
        g (fn [x y] (hash-map x y))
        z (juxt f g)]
    (z :k1 10)))
(defn función-juxt-14
  []
  (let [f (fn [w x y] (* x y w))
        g (fn [w x y] (/ x y w))
        z (juxt f g)]
    (z 100 10 1)))
(defn función-juxt-15
  []
  (let [f (fn [xs] (filter string? xs))
        g (fn [xs] (filter number? xs))
        z (juxt f g)]
    (z #{"UNO" "TWO" "TEN" 1 2 10})))
(defn función-juxt-16
  []
  (let [f (fn [xs] (filter float? xs))
        g (fn [xs] (filter int? xs))
        z (juxt f g)]
    (z #{1.0 2.0 10.0 30.5 1 2 10 30 "1"} )))
(defn función-juxt-17 
  []
  (let [f (fn [x y] (+ x y))
        g (fn [x y] (- x y))
        h (fn [x y] (= x y))
        z (juxt f g h)]
    (z 30 -30)))
(defn función-juxt-18
  []
  (let [f (fn [x y] (boolean? (= x y)))
        g (fn [x y] (vector x y))
        z (juxt f g)]
    (z 100 100)))
(defn función-juxt-19
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [xs] (map inc xs))
        z (juxt f g)]
    (z [1 2 3 4 5 -1 -2 -3 -4 -5])))
(defn función-juxt-20
  []
  (let [f (fn [xs] (map inc xs))
        g (fn [xs] (map dec xs))
        z (juxt f g)]
    (z [10 20 30 40 50])))
(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)
;;partial-10
(defn función-partial-1
  []
  (let [f (fn [w x y] (vector w x y ))
        z (partial f 1)]
    (z "DAN" "LM")))
(defn función-partial-2
  []
  (let [f (fn [v w x y] (list v w x y))
        z (partial f 100 0)]
    (z 1/2 90)))
(defn función-partial-3
  []
  (let [f (fn [v w x ys] (concat v w x ys))
        z (partial f "F")]
    (z "EN" "EL" ["CHAT" "RESPECT"])))
(defn función-partial-4
  []
  (let [f (fn [v w x ] (str v w x ))
        z (partial f true)]
    (z " false" " not boolean")))
(defn función-partial-5
  []
  (let [f (fn [v w x] (* v w x))
        z (partial f 90)]
    (z 0 1000)))
(defn función-partial-6
  []
  (let [f (fn [v w x] (+ v w x))
        z (partial f 90)]
    (z -10 1000)))
(defn función-partial-7
  []
  (let [f (fn [v w x] (< v w x))
        z (partial f 1)]
    (z 2 3)))
(defn función-partial-8
  []
  (let [f (fn [v w x y] (max v w x y))
        z (partial f 1)]
    (z 1000 200 -10000)))
(defn función-partial-9
  []
  (let [f (fn [u v w x y] (str u v w x y))
        z (partial f "you ")]
    (z "can " \d \o " it")))
(defn función-partial-10
  []
  (let [f (fn [xs ys] (concat xs ys))
        z (partial f ["tu puedes "])]
    (z '("hacerlo ..."))))
;;partial-20
(defn función-partial-11
  []
  (let [f (fn [x y] (= x y))
        z (partial f 90)]
    (z 90.00001)))
(defn función-partial-12
  []
  (let [f (fn [x y] (vector (* x y)(+ x y)))
        z (partial f 100)]
    (z 0.001)))
(defn función-partial-13
  []
  (let [f (fn [x y] (vector (* x y) (+ x y) (- x y)))
        z (partial f 9)]
    (z 0.009)))
(defn función-partial-14
  []
  (let [f (fn [x y] (list (* x y) (+ x y) (- x y) (/ x y)))
        z (partial f 98.1)]
    (z 8.1)))
(defn función-partial-15
  []
  (let [f (fn [x y] (hash-map x y))
        z (partial f :k1)]
    (z "Nuevo valor")))
(defn función-partial-16
  []
  (let [f (fn [x y w v] (hash-map x y w v))
        z (partial f :k1 :k2)]
    (z 80 -80)))
(defn función-partial-17
  []
  (let [f (fn [x y] (list (* x x) (* y y)))
        z (partial f 10)]
    (z 20)))
(defn función-partial-18
  []
  (let [f (fn [x y] (list (reverse x) (reverse y)))
        z (partial f "JOSE")]
    (z "ESOJ")))
(defn función-partial-19
  []
  (let [f (fn [x y] (list (str x y)))
        z (partial f "LOS ")]
    (z "ACOSTA")))
(defn función-partial-20
  []
  (let [f (fn [w x y] (vector (str w x y)))
        z (partial f \O)]
    (z \S \O)))
(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)
;;some-fn
(defn función-some-fn-1
  []
  (let [f (fn [w] (even? w))
        z (some-fn f)]
    (z 11)))
(defn función-some-fn-2
  []
  (let [f (fn [w] (odd? w))
        z (some-fn f)]
    (z 2211)))
(defn función-some-fn-3
  []
  (let [f (fn [w] (number? w))
        g (fn [w] (odd? w))
        z (some-fn g f)]
    (z 21122)))
(defn función-some-fn-4
  []
  (let [f (fn [w] (boolean? w))
        g (fn [w] (string? w))
        z (some-fn g f)]
    (z -10)))
(defn función-some-fn-5
  []
  (let [f (fn [w] (coll? w))
        g (fn [w] (int? w))
        z (some-fn g f)]
    (z [19])))
(defn función-some-fn-6
  []
  (let [f (fn [w] (ratio? w))
        g (fn [w] (double? w))
        z (some-fn g f)]
    (z 1.2342)))
(defn función-some-fn-7
  []
  (let [f (fn [w] (int? w))
        g (fn [w] (double? w))
        h (fn [w] (float? w))
        z (some-fn f g h)]
    (z 1/3)))
(defn función-some-fn-8
  []
  (let [f (fn [w] (int? w))
        g (fn [w] (double? w))
        h (fn [w] (float? w))
        i (fn [w] (boolean? w))
        z (some-fn f g h i)]
    (z false)))
(defn función-some-fn-9
  []
  (let [f (fn [w] (list? w))
        g (fn [w] (vector? w))
        z (some-fn f g)]
    (z [30 40])))
(defn función-some-fn-10
  []
  (let [f (fn [w] (nat-int? w))
        g (fn [w] (int? w))
        z (some-fn f g)]
    (z -30)))
;;some-fn-20
(defn función-some-fn-11
  []
  (let [f (fn [w] (keyword? w))
        z (some-fn f)]
    (z :k1)))
(defn función-some-fn-12
  []
  (let [f (fn [w] (keyword? w))
        g (fn [w] (number? w))
        z (some-fn f g)]
    (z :1090)))
(defn función-some-fn-13
  []
  (let [f (fn [w] (rational? w))
        g (fn [w] (neg? w))
        z (some-fn f g)]
    (z -1/2)))
(defn función-some-fn-14
  []
  (let [f (fn [w] (seq? w))
        g (fn [w] (list? w))
        z (some-fn f g)]
    (z #{10 30 20})))
(defn función-some-fn-15
  []
  (let [f (fn [w] (seq? w))
        g (fn [w] (seqable? w))
        z (some-fn f g)]
    (z [1 2 5 4 3])))
(defn función-some-fn-16
  []
  (let [f (fn [w] (true? w))
        g (fn [w] (boolean? w))
        z (some-fn f g)]
    (z false)))
(defn función-some-fn-17
  []
  (let [f (fn [w] (false? w))
        g (fn [w] (integer? w))
        z (some-fn f g)]
    (z -1111)))
(defn función-some-fn-18
  []
  (let [f (fn [w] (set? w))
        g (fn [w] (map? w))
        z (some-fn f g)]
    (z [4 3 2 1])))
(defn función-some-fn-19
  []
  (let [f (fn [w] (vector? w))
        g (fn [w] (map? w))
        z (some-fn f g)]
    (z {:k1 10 :k2 20})))
(defn función-some-fn-20
  []
  (let [f (fn [w] (keyword? w))
        g (fn [w] (string? w))
        h (fn [w] (boolean? w))
        z (some-fn f g h)]
    (z 12)))
(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)



